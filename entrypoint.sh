#!/bin/bash
sleep 5

cd /home/container

ulimit -n 4096

export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:$(pwd):/home/container/RustDedicated_Data/Plugins/x86_64"
# Update Rust Server
./steam/steamcmd.sh +login anonymous +force_install_dir /home/container +app_update 258550 +quit

if [ -f OXIDE_FLAG ] || [ "${OXIDE}" = 1 ]; then
    echo "Updating OxideMod..."
    curl -sSL "http://releases.ooplay.cn/releases/Oxide-Rust.zip" > oxide.zip
    unzip -o -q oxide.zip
    rm oxide.zip
    echo "Done updating OxideMod!"
fi

# Replace Startup Variables
MODIFIED_STARTUP=$(eval echo $(echo ${STARTUP} | sed -e 's/{{/${/g' -e 's/}}/}/g'))
echo ":/home/container$ ${MODIFIED_STARTUP}"

# Run the Server
${MODIFIED_STARTUP}
echo "服务器退出了,如果是管理操作的请忽略此消息!"