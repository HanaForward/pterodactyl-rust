FROM ubuntu:18.04

MAINTAINER Hana, <Hana@1586606701@qq.com>


RUN apt update \
    && apt upgrade -y  \
    && apt install -y lib32gcc1 libgdiplus lib32stdc++6 unzip curl \
    && useradd -d /home/container -m container


USER container
ENV  USER=container HOME=/home/container

WORKDIR /home/container

COPY ./entrypoint.sh /entrypoint.sh


CMD ["/bin/bash", "/entrypoint.sh"]
